package cv5;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import oglutils.OGLBuffers;
import oglutils.OGLRenderTarget;
import oglutils.OGLTextRenderer;
import oglutils.OGLTexture2D;
import oglutils.OGLUtils;
import oglutils.ShaderUtils;
import oglutils.ToFloatArray;
import transforms.Camera;
import transforms.Mat4;
import transforms.Mat4OrthoRH;
import transforms.Mat4PerspRH;
import transforms.Mat4ViewRH;
import transforms.Vec3D;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * GLSL sample:<br/>
 * Read and compile shader from files "/shader/glsl01/start.*" using ShaderUtils
 * class in oglutils package (older GLSL syntax can be seen in
 * "/shader/glsl01/startForOlderGLSL")<br/>
 * Manage (create, bind, draw) vertex and index buffers using OGLBuffers class
 * in oglutils package<br/>
 * Requires JOGL 2.3.0 or newer
 *
 * @author PGRF FIM UHK
 * @version 2.0
 * @since 2015-09-05
 */
public class Renderer implements GLEventListener, MouseListener,
        MouseMotionListener, KeyListener {

    private int width, height, ox, oy;

    private OGLBuffers buffers;
    private OGLTextRenderer textRenderer;
    private OGLRenderTarget renderTarget;

    private int shaderProgram1, shaderProgram2, locTime, locMatV, locMatP;

    private float time = 0;

    // Matice pro vykreslení sceny - z pohledu pozorovatele:
    // nahoru bude x - posledni vektor:
    private Mat4 matView = new Mat4ViewRH(new Vec3D(10, 10, 10), new Vec3D(-1, -1, -1), new Vec3D(0, 0, 1));
    private Mat4 matProj = new Mat4OrthoRH(5, 5, 0.1, 20);

    // Matice pro vykresleni z pozice svetla:
    private Mat4 lightProj = new Mat4OrthoRH(5, 5, 0.1, 20);
    // Bude napravo po ose X, bude se dívat do středu -X
    // TODO - to zakomentovane by melo fungovat, protoze pak bude potreba pohybovat svetlem!, docasne nahrazeno tou kamerou nize
//    private Mat4 lightView = new Mat4ViewRH(new Vec3D(10, 10, 10), new Vec3D(-1, -1, -1), new Vec3D(0, 0, 1));
    private Mat4 lightView = new Camera().withPosition(new Vec3D(10, 10, 10))
//            .addAzimuth(1.25*Math.PI)
//            .addZenith(-0.125*Math.PI)
            .addAzimuth(5./4*Math.PI)
            .addZenith(-1./4*Math.PI)
            .withRadius(5)
            .getViewMatrix();

    // fixme vyse lightView opravit tak, aby se divala na teleso, tomu mozna i prizpusobit matView a kameru? (dle toho, jak se nastavit lightView se budou vrhat stiny), asi se zeptat na souradnice Jezka

    /**
     * Zdali se jedná o perspektivní (true) nebo paralelní / orthogonální (false) projekci.
     */
    private boolean perspectiveProj = false;

    // azumut je vůči ose x:
    private Camera cam = new Camera().withPosition(new Vec3D(10, 10, 10))
            .addAzimuth(5./4*Math.PI)
            .addZenith(-1./4*Math.PI);
    // Budu se dívat pořád do středu: jak asi chodit jakoby okolo:
//    Camera cam = new Camera().withPosition(new Vec3D(0, 0, 0))
//            .addAzimuth(Math.PI * 1.25)
//            .addZenith(Math.PI * -0.125).withFirstPerson(false).withRadius(5);

    private OGLTexture2D texture;

    private OGLTexture2D.Viewer textureViewer;

    @Override
    public void init(GLAutoDrawable glDrawable) {
        // check whether shaders are supported
        GL2GL3 gl = glDrawable.getGL().getGL2GL3();
        OGLUtils.shaderCheck(gl);

        OGLUtils.printOGLparameters(gl);

        textRenderer = new OGLTextRenderer(gl, glDrawable.getSurfaceWidth(), glDrawable.getSurfaceHeight());

        // shader files are in /shaders/ directory
        // shaders directory must be set as a source directory of the project
        // e.g. in Eclipse via main menu Project/Properties/Java Build Path/Source

        // na konci roliseni textury, do ktere se renderuje
//        renderTarget = new OGLRenderTarget(gl, 1024, 1024);
        renderTarget = new OGLRenderTarget(gl, 256, 256);

        shaderProgram1 = ShaderUtils.loadProgram(gl, "/cv5/light");
        shaderProgram2 = ShaderUtils.loadProgram(gl, "/cv5/scene");

        //shorter version of loading shader program
        //shaderProgram = ShaderUtils.loadProgram(gl, "/lvl1basic/p01start/p04utils/start");

        //for older GLSL version
        //shaderProgram = ShaderUtils.loadProgram(gl, "/lvl1basic/p01start/p04utils/startForOlderGLSL");

        texture = new OGLTexture2D(gl, "/textures/mosaic.jpg");


        buffers = GridFactory.createGrid(gl, 100, 100);

        textureViewer = new OGLTexture2D.Viewer(gl);
    }


    @Override
    public void display(GLAutoDrawable glDrawable) {
        GL2GL3 gl = glDrawable.getGL().getGL2GL3();

        // Zapnutí Z - bufferu:
        gl.glEnable(GL2GL3.GL_DEPTH_TEST);

        // fill je pro vyplněnou plochu
        gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, GL2GL3.GL_FILL);
//        gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, GL2GL3.GL_LINE);

        // set the current shader to be used, could have been done only once (in
        // init) in this sample (only one shader used)
        gl.glUseProgram(shaderProgram1);
        // Zde se začne renderovat do rendertargetu:
        renderTarget.bind();
        renderFromLight(gl, shaderProgram1);

        // vykreslení druhého shader programu:
        gl.glUseProgram(shaderProgram2);
        // Zde se má renderovat normálně do výstupu (specifikují následující dva řádky):
        // set the default render target (screen)
        gl.glBindFramebuffer(GL2GL3.GL_FRAMEBUFFER, 0);
        gl.glViewport(0, 0, width, height);
        renderFromViewer(gl, shaderProgram2);



        String text = this.getClass().getName();
        textRenderer.drawStr2D(3, height - 20, text);
        textRenderer.drawStr2D(width - 90, 3, " (c) PGRF UHK");

        // vykreslení textury v levem spodním kraji
        textureViewer.view(texture, -1, -1, 0.5);
        textureViewer.view(renderTarget.getColorTexture(), -1, -0.5, 0.5);
        textureViewer.view(renderTarget.getDepthTexture(), -1, 0, 0.5);
    }


    private void renderFromLight(GL2GL3 gl, int shaderProgram) {
        int locMode = gl.glGetUniformLocation(shaderProgram, "mode");
        // na konci se předávají parametry do start.vert - ty názvy!
        locTime = gl.glGetUniformLocation(shaderProgram, "time");
        int locLightView = gl.glGetUniformLocation(shaderProgram, "matLightView");;
        int locLightProj = gl.glGetUniformLocation(shaderProgram, "mathLightProj");;

        gl.glClearColor(0.3f, 0.0f, 0.0f, 1.0f);
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

        time += 0.1;
        gl.glUniform1f(locTime, time); // correct shader must be set before this
        // count je počet matic
        gl.glUniformMatrix4fv(locLightView, 1, false, ToFloatArray.convert(lightView), 0);
        gl.glUniformMatrix4fv(locLightProj, 1, false, ToFloatArray.convert(lightProj), 0);


        // bind and draw
        gl.glUniform1i(locMode, 1);
        buffers.draw(GL2GL3.GL_TRIANGLES, shaderProgram);

        gl.glUniform1i(locMode, 0);
        buffers.draw(GL2GL3.GL_TRIANGLES, shaderProgram);
    }


    private void renderFromViewer(GL2GL3 gl, int shaderProgram) {
        // na konci se předávají parametry do start.vert - ty názvy!
        locTime = gl.glGetUniformLocation(shaderProgram, "time");
        int locMode = gl.glGetUniformLocation(shaderProgram, "mode");
        locMatV = gl.glGetUniformLocation(shaderProgram, "matView");
        locMatP = gl.glGetUniformLocation(shaderProgram, "matProj");
        // view a projection dohromady:
        int locLightVP = gl.glGetUniformLocation(shaderProgram, "matLightVP");

        gl.glClearColor(0.0f, 0.3f, 0.0f, 1.0f);
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

        texture.bind(shaderProgram, "textureID", 0);
        // chceme bindovat jinou texturu:
        // JE TREBA NASTAVIT SLOT - PRO KAZDY SLOT JEDNA TEXTURA
        renderTarget.getDepthTexture().bind(shaderProgram, "textureDepth", 1);

        time += 0.1;
        gl.glUniform1f(locTime, time); // correct shader must be set before this
        // count je počet matic
        gl.glUniformMatrix4fv(locMatV, 1, false, ToFloatArray.convert(matView), 0);
        gl.glUniformMatrix4fv(locMatP, 1, false, ToFloatArray.convert(matProj), 0);
        gl.glUniformMatrix4fv(locLightVP, 1, false, ToFloatArray.convert(lightView.mul(lightProj)), 0);
        // pro nastavení kamery:


        // bind and draw
        gl.glUniform1i(locMode, 1);
        buffers.draw(GL2GL3.GL_TRIANGLES, shaderProgram);

        gl.glUniform1i(locMode, 0);
        buffers.draw(GL2GL3.GL_TRIANGLES, shaderProgram);
    }


    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width,
                        int height) {
        this.width = width;
        this.height = height;
        // Nastavení projekční matice pouze v případě, že je nastavena perspektivní projekce, jinak by se při ortogonální projekci nastavila perspektivní:
        if (perspectiveProj)
            lightProj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.01, 1000.0);
        textRenderer.updateSize(width, height);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        ox = e.getX();
        oy = e.getY();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        cam = cam.addAzimuth(Math.PI * (ox - e.getX()) / width)
                .addZenith(Math.PI * (e.getY() - oy) / width);
        ox = e.getX();
        oy = e.getY();

//        lightView = cam.getViewMatrix();
        matView = cam.getViewMatrix();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_W:
                cam = cam.forward(1);
                break;
            case KeyEvent.VK_D:
                cam = cam.right(1);
                break;
            case KeyEvent.VK_S:
                cam = cam.backward(1);
                break;
            case KeyEvent.VK_A:
                cam = cam.left(1);
                break;
            case KeyEvent.VK_CONTROL:
                cam = cam.down(1);
                break;
            case KeyEvent.VK_SHIFT:
                cam = cam.up(1);
                break;
            case KeyEvent.VK_SPACE:
                cam = cam.withFirstPerson(!cam.getFirstPerson());
                break;
            case KeyEvent.VK_R:
                cam = cam.mulRadius(0.9f);
                break;
            case KeyEvent.VK_F:
                cam = cam.mulRadius(1.1f);
                break;
            case KeyEvent.VK_P:
                // Změna typu projekce:
                perspectiveProj = !perspectiveProj;

                if (perspectiveProj)
                    lightProj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.01, 20.0);
                else
                    lightProj = new Mat4OrthoRH(5, 5, 0.1, 20);
                break;
        }
//        lightView = cam.getViewMatrix();
        matView = cam.getViewMatrix();
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void dispose(GLAutoDrawable glDrawable) {
        GL2GL3 gl = glDrawable.getGL().getGL2GL3();
        gl.glDeleteProgram(shaderProgram1);
        gl.glDeleteProgram(shaderProgram2);
    }
}