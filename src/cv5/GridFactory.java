package cv5;

import com.jogamp.opengl.GL2GL3;
import oglutils.OGLBuffers;
import oglutils.ToIntArray;

import java.util.ArrayList;
import java.util.List;

public class GridFactory {

    public static OGLBuffers createGrid(GL2GL3 gl3, int m, int n) {
        final float[] vb = new float[m * n * 2];

        int index = 0;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {

                System.out.println("i: " + i + ", j: " + j);

                System.out.println(1. / (m - 1) * i);
                System.out.println(1. / (n - 1) * j);

                vb[index++] = (float) 1. / (m - 1) * i;
                vb[index++] = (float) 1. / (n - 1) * j;
            }
        }

        List<Integer> indices = new ArrayList<>();

        for (int r = 0; r < m - 1; r++) {
            for (int c = 0; c < n - 1; c++) {
                indices.add(r * n + c);
                indices.add(r * n + c + 1);
                indices.add(r * n + c + n);

                indices.add(r * n + 1 + c);
                indices.add(r * n + n + c + 1);
                indices.add(r * n + c + n);
            }
        }

        OGLBuffers.Attrib[] attributes = {
                new OGLBuffers.Attrib("inPosition", 2), // 2 floats
        };
        return new OGLBuffers(gl3, vb, attributes,
                              ToIntArray.convert(indices)
        );
    }
}