package cv3;

import com.jogamp.opengl.GL2GL3;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import oglutils.*;
import transforms.*;

import java.awt.event.*;

/**
 * GLSL sample:<br/>
 * Read and compile shader from files "/shader/glsl01/start.*" using ShaderUtils
 * class in oglutils package (older GLSL syntax can be seen in
 * "/shader/glsl01/startForOlderGLSL")<br/>
 * Manage (create, bind, draw) vertex and index buffers using OGLBuffers class
 * in oglutils package<br/>
 * Requires JOGL 2.3.0 or newer
 *
 * @author PGRF FIM UHK
 * @version 2.0
 * @since 2015-09-05
 */
public class Renderer implements GLEventListener, MouseListener,
        MouseMotionListener, KeyListener {

    int width, height, ox, oy;

    OGLBuffers buffers;
    OGLTextRenderer textRenderer;

    int shaderProgram, locTime, locMatV, locMatP;

    float time = 0;

    // nahoru bude x - posledni vektor:
//    private Mat4 matView = new Mat4ViewRH(new Vec3D(10, 10, 10), new Vec3D(-1, -1, -1), new Vec3D(1, 0, 0));

    private Mat4 matProj = new Mat4OrthoRH(5, 5, 0.1, 20);
    /**
     * Zdali se jedná o perspektivní (true) nebo paralelní / orthogonální (false) projekci.
     */
    private boolean perspectiveProj = false;

    // azumut je vůči ose x:
    Camera cam = new Camera().withPosition(new Vec3D(10, 10, 10))
            .addAzimuth(5./4*Math.PI)
            .addZenith(-1./4*Math.PI);

    OGLTexture2D texture;

    OGLTexture2D.Viewer textureViewer;

    @Override
    public void init(GLAutoDrawable glDrawable) {
        //		GridFactory.createGrid(4, 3);

        // check whether shaders are supported
        GL2GL3 gl = glDrawable.getGL().getGL2GL3();
        OGLUtils.shaderCheck(gl);

        OGLUtils.printOGLparameters(gl);

        textRenderer = new OGLTextRenderer(gl, glDrawable.getSurfaceWidth(), glDrawable.getSurfaceHeight());

        // shader files are in /shaders/ directory
        // shaders directory must be set as a source directory of the project
        // e.g. in Eclipse via main menu Project/Properties/Java Build Path/Source


        shaderProgram = ShaderUtils.loadProgram(gl, "/cv3/start.vert",
                                                "/cv3/start.frag",
                                                null, null, null, null
        );

        //shorter version of loading shader program
        //shaderProgram = ShaderUtils.loadProgram(gl, "/lvl1basic/p01start/p04utils/start");

        //for older GLSL version
        //shaderProgram = ShaderUtils.loadProgram(gl, "/lvl1basic/p01start/p04utils/startForOlderGLSL");

        texture = new OGLTexture2D(gl, "/textures/mosaic.jpg");


//        buffers = GridFactory.createGrid(gl, 10, 8);
        buffers = GridFactory.createGrid(gl, 100, 100);
//        buffers = GridFactory.createGrid(gl, 4, 4);

        //		createBuffers(gl);

        // na konci se předávají parametry do start.vert - ty názvy!
        locTime = gl.glGetUniformLocation(shaderProgram, "time");
        locMatV = gl.glGetUniformLocation(shaderProgram, "matView");
        locMatP = gl.glGetUniformLocation(shaderProgram, "matProj");

        textureViewer = new OGLTexture2D.Viewer(gl);
    }

    void createBuffers(GL2GL3 gl) {
        float[] vertexBufferData = {
                -1, -1, 0.7f, 0, 0,
                1, 0, 0, 0.7f, 0,
                0, 1, 0, 0, 0.7f
        };
        int[] indexBufferData = {0, 1, 2};

        // vertex binding description, concise version
        OGLBuffers.Attrib[] attributes = {
                new OGLBuffers.Attrib("inPosition", 2), // 2 floats
                new OGLBuffers.Attrib("inColor", 3) // 3 floats
        };
        buffers = new OGLBuffers(gl, vertexBufferData, attributes,
                                 indexBufferData
        );
        // the concise version requires attributes to be in this order within
        // vertex and to be exactly all floats within vertex

/*		full version for the case that some floats of the vertex are to be ignored 
 * 		(in this case it is equivalent to the concise version): 
 		OGLBuffers.Attrib[] attributes = {
				new OGLBuffers.Attrib("inPosition", 2, 0), // 2 floats, at 0 floats from vertex start
				new OGLBuffers.Attrib("inColor", 3, 2) }; // 3 floats, at 2 floats from vertex start
		buffers = new OGLBuffers(gl, vertexBufferData, 5, // 5 floats altogether in a vertex
				attributes, indexBufferData); 
*/
    }


    @Override
    public void display(GLAutoDrawable glDrawable) {
        GL2GL3 gl = glDrawable.getGL().getGL2GL3();

        gl.glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
        /*
         * zapnutí Z - bufferu:
         *
         * bez toho by se moly vykreslovat odvracene vrcholy, které jsou v zadu a neměly by být vidět, ale vykreslují se později - po tech ve předu.
         */
        gl.glEnable(GL2GL3.GL_DEPTH_TEST);
        gl.glClear(GL2GL3.GL_COLOR_BUFFER_BIT | GL2GL3.GL_DEPTH_BUFFER_BIT);

        // fill je pro vyplněnou plochu
        gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, GL2GL3.GL_FILL);
//        gl.glPolygonMode(GL2GL3.GL_FRONT_AND_BACK, GL2GL3.GL_LINE);

        // set the current shader to be used, could have been done only once (in
        // init) in this sample (only one shader used)
        gl.glUseProgram(shaderProgram);

        texture.bind(shaderProgram, "textureID", 0);

        time += 0.1;
        gl.glUniform1f(locTime, time); // correct shader must be set before this
        // count je počet matic
//        gl.glUniformMatrix4fv(locMatV, 1, false, ToFloatArray.convert(matView), 0);
        gl.glUniformMatrix4fv(locMatV, 1, false, ToFloatArray.convert(cam.getViewMatrix()), 0);

        gl.glUniformMatrix4fv(locMatP, 1, false, ToFloatArray.convert(matProj), 0);
        // pro nastavení kamery:


        // bind and draw
        buffers.draw(GL2GL3.GL_TRIANGLES, shaderProgram);

        String text = new String(this.getClass().getName());
        textRenderer.drawStr2D(3, height - 20, text);
        textRenderer.drawStr2D(width - 90, 3, " (c) PGRF UHK");

        // vykreslení textury v levem spodním kraji
        // Obrázek bude úplně dole v levé části, ale bude "velký"
//        textureViewer.view(texture);
        // Obrázek bude posunut po ose y ("nebude úplně dole, ale kousek nad okrajem") a zmenšena velikost
//        textureViewer.view(texture, -1, -0.5, 0.5);
        // Obrázek bude úplně dole ve spodní části okna aplikace a zmenšena velikost:
        textureViewer.view(texture, -1, -1, 0.5);

    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width,
                        int height) {
        this.width = width;
        this.height = height;
        // Nastavení projekční matice pouze v případě, že je nastavena perspektivní projekce, jinak by se při ortogonální projekci nastavila perspektivní:
        if (perspectiveProj)
            matProj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.01, 1000.0);
        textRenderer.updateSize(width, height);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        ox = e.getX();
        oy = e.getY();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        cam = cam.addAzimuth(Math.PI * (ox - e.getX()) / width)
                .addZenith(Math.PI * (e.getY() - oy) / width);
        ox = e.getX();
        oy = e.getY();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_W:
                cam = cam.forward(1);
                break;
            case KeyEvent.VK_D:
                cam = cam.right(1);
                break;
            case KeyEvent.VK_S:
                cam = cam.backward(1);
                break;
            case KeyEvent.VK_A:
                cam = cam.left(1);
                break;
            case KeyEvent.VK_CONTROL:
                cam = cam.down(1);
                break;
            case KeyEvent.VK_SHIFT:
                cam = cam.up(1);
                break;
            case KeyEvent.VK_SPACE:
                cam = cam.withFirstPerson(!cam.getFirstPerson());
                break;
            case KeyEvent.VK_R:
                cam = cam.mulRadius(0.9f);
                break;
            case KeyEvent.VK_F:
                cam = cam.mulRadius(1.1f);
                break;
            case KeyEvent.VK_P:
                // Změna typu projekce:
                perspectiveProj = !perspectiveProj;

                if (perspectiveProj)
                    matProj = new Mat4PerspRH(Math.PI / 4, height / (double) width, 0.01, 20.0);
                else matProj = new Mat4OrthoRH(5, 5, 0.1, 20);
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void dispose(GLAutoDrawable glDrawable) {
        GL2GL3 gl = glDrawable.getGL().getGL2GL3();
        gl.glDeleteProgram(shaderProgram);
    }
}