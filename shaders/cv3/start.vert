#version 150
in vec2 inPosition; // input from the vertex buffer
out vec3 vertColor; // output from this shader to the next pipeline stage
out vec3 normal;// normála té sphéry
out vec2 textCoord;
uniform float time; // variable constant for all vertices in a single draw
uniform mat4 matView;
uniform mat4 matProj;
float PI = 3.1459;
// Zdroj světla (vpravo nahoře za uchem)
vec3 lightPos = vec3(5, 5, 1);

// GEOMETRII OHYBAME VE VERTEXSHADERU!

float getZ(float x, float y) {// prepsat jen na ten vektor v parametru
// funkce pro vlneni: Z = sin(x*2*PI) - uvnitr sinu je ještě time kvůli vlneni!
return sin((x * 2 * 3.14) + time);
}



vec3 doNormal(in vec2 pos){
vec3 normal;

// ZDE NEJAK DODELAT DERIVACI TE FUNKCE VYSE! viz prednasky

//float distance2 = pos.x*pos.x+pos.y*pos.y;
//normal.x = -scale*PI*sin(sqrt(distance2))/distance2*pos.x/2.0;
//normal.y = -scale*PI*sin(sqrt(distance2))/distance2*pos.y/2.0;
//normal.z = 1.0;
return normal;
}


// prepocet na spherické souradnice
// - melo by generovat nejakou spheru:

// je treba, aby bylo okno jako ctverec - kvůli projekci - bylo by treba upravit metodu reshape, aby to fungovalo normalne!
vec3 getSphere(vec2 xy) {
float azimut = xy.x * PI;
float zenit = xy.y * PI/2;
//float r = 1;

float x = cos(azimut)*cos(zenit);
float y = sin(azimut)*cos(zenit);
float z = sin(zenit);
return vec3(x, y, z);
}


// získání normály
vec3 getNormalSphere(vec2 xy) {
float azimut = xy.x * PI;
float zenit = xy.y * PI/2;

// derivace funkce v getSphere:  cos(azimut)*cos(zenit);:
vec3 dx = vec3(-sin(azimut)*cos(zenit)*PI, cos(azimut)*cos(zenit) * PI, 0);
// derivace  cos(azimut)*cos(zenit); dle y:
vec3 dy = vec3(cos(azimut)*(-sin(zenit)) * PI/2, sin(azimut) * (-sin(zenit) * PI/2), cos(zenit) * PI/2);

return cross(dx, dy);
}


void main() {
    vec2 pos;

    // nasledujici operace jsou jedno a to samé, pouze jinak napsané - přepočet do okna na rozsah od -1 do 1

     //pos.x = inPosition.x*2-1;
     //pos.y = inPosition.y*2-1;
    // nebo:
     //pos = inPosition*vec2(2)-vec2(1, 1);
    // nebo:
     pos = inPosition*2 - 1;

     float z = getZ(pos.x, pos.y);
     vec4 pos4 = vec4(pos, z, 1.0);

     pos4 = vec4(getSphere(pos), 1.0);

     vec3 normal = doNormal(pos.xy);


//	gl_Position = pos4 * matView * matProj;
	gl_Position = matProj * matView * pos4;// toto je spravně

	// obarvení:
	vertColor = pos4.xyz;
	// normála té sphéry:
	normal = inverse(transpose(mat3(matView))) * pos4.xyz;

	vec3 l = lightPos - (matView * pos4).xyz;
	vertColor = vec3(dot(normalize(normal), normalize(l)));

	// fixme - vektor v je -objmodelu ci co? Nějak ho dopocitat

	// fixme - DU - p04target, podívat se na p02utils

	// Souřadníce do textury bez jakýchkoli transformací!:
	textCoord = inPosition;
}