#version 150
// in hodnoty prijdou z start.fraf - vertex shaderu
in vec3 vertColor; // input from the previous pipeline stage
in vec2 textCoord;
out vec4 outColor; // output from the fragment shader
in vec3 normal; //input normal - normálový vektor
uniform sampler2D textureID;

// Zdroj světla:
vec3 posLight;

void main() {
    posLight = vec3(0, 0, 10); // zdroj světla bude někde nad gridem - nad stredem sceny, jen X je nahore

	outColor = vec4(vertColor, 1.0);// - OK, ale není cerna

    // namapování textury:
	outColor = texture(textureID, textCoord);
	// tímto bude koule i osvětlená difuzní složkou:
	outColor.xyz = texture(textureID, textCoord).xyz * vertColor.xyz;

// fixme - následujici bylo odkomentovane, ale uz nevim proc nebo k cemu, ted je to zakomentovano, aby se vykreslila sphera

//	float f = dot(normalize(vec3(0.0,1.0,1.0)), normal);
//    f = max(f,0.0);
//
//outColor.rgb = vec3(f);
//// alpha (opacity) must be 1.0
//outColor.a = 1.0;
}