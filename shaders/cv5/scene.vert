#version 150
in vec2 inPosition; // input from the vertex buffer
out vec3 vertColor; // output from this shader to the next pipeline stage
out vec3 normal;// normála té sphéry
out vec2 textCoord;
out vec4 depthTextCoord;
uniform float time; // variable constant for all vertices in a single draw
uniform mat4 matView;
uniform mat4 matProj;
uniform mat4 matLightVP;
uniform int mode;
float PI = 3.1459;

// Zdroj světla (vpravo nahoře za uchem)
vec3 lightPos = vec3(5, 5, 1);


// prepocet na spherické souradnice
// - melo by generovat nejakou spheru:

// je treba, aby bylo okno jako ctverec - kvůli projekci - bylo by treba upravit metodu reshape, aby to fungovalo normalne!
vec3 getSphere(vec2 xy) {
float azimut = xy.x * PI;
float zenit = xy.y * PI/2;
//float r = 1;

float x = 2*cos(azimut)*cos(zenit);
float y = 0.5*sin(azimut)*cos(zenit);
float z = sin(zenit);
return vec3(x, y, z);
}


void main() {
    vec2 pos = inPosition*2 - 1;

    vec4 pos4;

    if (mode == 1) {
        pos4 = vec4(getSphere(pos), 1.0);
        normal = inverse(transpose(mat3(matView))) * pos4.xyz;
     }
     else {
        pos4 = vec4(pos, 1, 1.0);
        normal = inverse(transpose(mat3(matView))) * vec3(0, 0, 1);
      }

	gl_Position = matProj * matView * pos4;// toto je spravně (v tomto pořadí, protože se matice při vstupu do shaderu transponuje)

    depthTextCoord = matLightVP * pos4;
    depthTextCoord.xyz = (depthTextCoord.xyz + 1.) / 2.;

	// obarvení:
	vertColor = pos4.xyz;
	// normála té sphéry:
	normal = inverse(transpose(mat3(matView))) * pos4.xyz;

	vec3 l = lightPos - (matView * pos4).xyz;
	vertColor = vec3(dot(normalize(normal), normalize(l)));

	// fixme - vektor v je -objmodelu ci co? Nějak ho dopocitat

	// Souřadníce do textury bez jakýchkoli transformací!:
	textCoord = inPosition;
}